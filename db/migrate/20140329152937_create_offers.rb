class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
	  t.string :offerTitle
      t.string :offerCos
      t.string :offerPicture
      t.date :offerStart_date
      t.date :offerEnd_date
      t.string :offerType_building_id
      t.string :offerArea
      t.integer :offerRooms
      t.string :offerLocalization
      t.string :offerMail_contact
      t.string :offerPhone_contact
      t.string :offerDescription

      t.timestamps
    end
  end
end
