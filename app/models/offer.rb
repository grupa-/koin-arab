class Offer < ActiveRecord::Base
  mount_uploader :offerPicture, PictureUploader
  belongs_to :user
end
