class RankOfUsersController < ApplicationController
  before_action :set_rank_of_user, only: [:show, :edit, :update, :destroy]

  # GET /rank_of_users
  # GET /rank_of_users.json
  def index
    @rank_of_users = RankOfUser.all
  end

  # GET /rank_of_users/1
  # GET /rank_of_users/1.json
  def show
  end

  # GET /rank_of_users/new
  def new
    @rank_of_user = RankOfUser.new
  end

  # GET /rank_of_users/1/edit
  def edit
  end

  # POST /rank_of_users
  # POST /rank_of_users.json
  def create
    @rank_of_user = RankOfUser.new(rank_of_user_params)

    respond_to do |format|
      if @rank_of_user.save
        format.html { redirect_to @rank_of_user, notice: 'Rank of user was successfully created.' }
        format.json { render action: 'show', status: :created, location: @rank_of_user }
      else
        format.html { render action: 'new' }
        format.json { render json: @rank_of_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rank_of_users/1
  # PATCH/PUT /rank_of_users/1.json
  def update
    respond_to do |format|
      if @rank_of_user.update(rank_of_user_params)
        format.html { redirect_to @rank_of_user, notice: 'Rank of user was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @rank_of_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rank_of_users/1
  # DELETE /rank_of_users/1.json
  def destroy
    @rank_of_user.destroy
    respond_to do |format|
      format.html { redirect_to rank_of_users_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rank_of_user
      @rank_of_user = RankOfUser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rank_of_user_params
      params[:rank_of_user]
    end
end
