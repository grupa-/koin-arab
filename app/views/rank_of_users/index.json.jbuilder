json.array!(@rank_of_users) do |rank_of_user|
  json.extract! rank_of_user, :id
  json.url rank_of_user_url(rank_of_user, format: :json)
end
