require 'test_helper'

class RankOfUsersControllerTest < ActionController::TestCase
  setup do
    @rank_of_user = rank_of_users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:rank_of_users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create rank_of_user" do
    assert_difference('RankOfUser.count') do
      post :create, rank_of_user: {  }
    end

    assert_redirected_to rank_of_user_path(assigns(:rank_of_user))
  end

  test "should show rank_of_user" do
    get :show, id: @rank_of_user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @rank_of_user
    assert_response :success
  end

  test "should update rank_of_user" do
    patch :update, id: @rank_of_user, rank_of_user: {  }
    assert_redirected_to rank_of_user_path(assigns(:rank_of_user))
  end

  test "should destroy rank_of_user" do
    assert_difference('RankOfUser.count', -1) do
      delete :destroy, id: @rank_of_user
    end

    assert_redirected_to rank_of_users_path
  end
end
